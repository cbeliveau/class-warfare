﻿//Chris Beliveau 2017
//For the LULZ

using System;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {
            //get number of users
            uint numPlayers = 0;
            bool done = false;
            Console.Write("Enter number of players: ");
            numPlayers = Convert.ToUInt16(Console.ReadLine());
            //player array
            Player[] pArr = new Player[numPlayers];

            //initialize players
            while (!done)
            {
                if (done) { break; }

                int foreach_count = 0;
                foreach (Player element in pArr)
                {
                    foreach_count += 1;
                    Console.WriteLine();
                    Console.WriteLine("Select Class: ");
                    Console.WriteLine("1. Standard Player (No Modifiers)");
                    Console.WriteLine("2. Wizard (2x Attack, 60% Defense)");
                    int pType = Convert.ToInt16(Console.ReadLine());
                    string pName;
                    switch (pType)
                    {
                        case 1:
                            Console.WriteLine("Selected class {0} ", pType);
                            Console.Write("Enter player name: ");
                            pName = Console.ReadLine();
                            pArr[foreach_count - 1] = new Player(pName, foreach_count);
                            break;
                        case 2:
                            Console.WriteLine("Selected class {0} ", pType);
                            Console.Write("Enter player name: ");
                            pName = Console.ReadLine();
                            pArr[foreach_count - 1] = new Wizard(pName, foreach_count);
                            break;
                        default:
                            Console.WriteLine("Invalid player class!");
                            Console.ReadLine();
                            return;
                    }

                    done = true;
                }


            }
            Console.WriteLine();
            Console.WriteLine("Battle Phase!");
            int battle_counter = 0;
            foreach (Player element in pArr)
            {
                battle_counter++;
                int hitcount = 0;
                while (!element.GetDeath())
                {
                    hitcount++;
                    Random baseDamage = new Random();
                    element.Attack(pArr[battle_counter - 1], baseDamage.Next(1, 20));
                    element.EvaluateDeath();
                    if(element.GetDeath())
                    {
                        Console.WriteLine("{0} has died after {1} hits with {2} HP!", element.name, hitcount, Math.Round(element.health));
                        Console.WriteLine();
                    }
                    System.Threading.Thread.Sleep(500);
                }


            }



            //keep console open
            Console.Read();
        }

    }


    class Player
    {
        //player properties
        public string name { get; set; }
        protected int id { get; set; }
        public double health { get; set; }
        protected bool isDead { get; set; }
        protected int hitCount { get; set; }
        protected double attackMod { get; set; }
        protected double defMod { get; set; }

        public double DefMod
        {
            get { return defMod; }
        }

        public double AttkMod
        {
            get { return attackMod; }
        }

        //null name, zero id player constructor
        public Player()
        {
            name = null;
            id = 0;
            health = 0;
            attackMod = 1;
            defMod = 1;
            //Console.WriteLine("Created blank player with null name and ID of 0.");
            //Console.WriteLine("You probably shouldn't see this...");
        }
        //player constructor w/ name and id
        public Player(string name, int id)
        {
            this.name = name;
            this.id = id;
            health = 100;
            attackMod = 1;
            defMod = 1;
            Console.WriteLine("Constructed player named " + name + " with ID of " + id + ". " + this.name + " has " + health + " HP.");
        }
        //return true if player should be dead
        public void EvaluateDeath()
        {
            if (health <= 0)
            {
                isDead = true;
            }
        }
        //return boolean value for player death
        public bool GetDeath()
        {
            if (isDead)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //remove health from player given attacker's attk mod and defender's defense mod
        public virtual void Damage(double amount, double aMod, double dMod)
        {
            hitCount++;
            double damage = amount * aMod * dMod;
            health -= damage;
            Console.WriteLine(name + " was hit for " + damage + " and has " + health + "HP.");
            EvaluateDeath();

            if (GetDeath())
            {
                Console.WriteLine(name + " has died.");
                Console.WriteLine();
            }

            System.Threading.Thread.Sleep(500);
        }

        public void Attack(Player target, double base_damage)
        {
            target.hitCount++;
            double damage = base_damage * AttkMod * target.DefMod;
            target.health -= damage;
            Console.WriteLine("{0} hit for {1} damage.", target.name, damage);
        }

        //dump player attributes
        public void printAttributes()
        {
            Console.WriteLine(name);
            Console.WriteLine(id);
            Console.WriteLine(health);
            Console.WriteLine(attackMod);
            Console.WriteLine(defMod);
        }

    }

    class Wizard : Player
    {
        //wizard constructors
        public Wizard()
        {
            name = null;
            id = 0;
            health = 0;
            attackMod = 1;
            defMod = 1;
            Console.WriteLine("Created blank wizard with null name and ID of 0.");
            Console.WriteLine("You probably shouldn't see this...");
        }

        public Wizard(string name, int id)
        {
            this.name = name;
            this.id = id;
            health = 100;
            attackMod = 2;
            defMod = 0.6;
            Console.WriteLine("Constructed Wizard named " + name + " with ID of " + id + ". " + this.name + " has " + health + " HP.");
        }
    }
}
