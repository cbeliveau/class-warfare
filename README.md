# Class Warfare
This game was written in an attempt to further my understanding of classes and inheritance within the C# language. The game features a base "Player" class, from which several specialized sub-classes are created.
The players engage in simulated combat according to their classes' modifiers.
### Implemented Player Classes
- Generic Player (No special modifiers.)
- Wizard (50% more attack power, but more succeptible to damage.)